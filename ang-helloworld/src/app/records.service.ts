import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RecordsService {

  info1: string [] = ["Adam Taylor", "Mavis Tractor ", "Bob Bar"]
  info2: string [] = ["Adam Taylor A", "Mavis Tractor A", "Bob Bar A"]
  info3: string [] = ["Adam Taylor B", "Mavis Tractor B", "Bob Bar B"]

  getInfo1(): string[] {
    return this.info1
  }
  getInfo2(): string[] {
    return this.info2
  }
  getInfo3(): string[] {
    return this.info3
  }
  constructor() { }
}
